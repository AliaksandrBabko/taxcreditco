package io.testpro.deens;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TaxCreditCoContactPageTest extends BaseTest {
    @DataProvider(name = "Authentication1")
    public static Object[][] credentials1() {
        return new Object[][]{{"test1", "test", "test@test.com ", "555-333-666", "Test", "55", "CA", "This is a test. Please ignore"}};
    }

    @Test(dataProvider = "Authentication1")
    public void testTax1(String Username, String JobTitle, String EMail, String Phone, String Company, String CompanySize, String StateRegion, String AdditionalInformation) {
        driver.findElement(By.name("firstname")).sendKeys(Username);
        driver.findElement(By.name("jobtitle")).sendKeys(JobTitle);
        driver.findElement(By.name("email")).sendKeys(EMail);
        driver.findElement(By.name("phone")).sendKeys(Phone);
        driver.findElement(By.name("company")).sendKeys(Company);
        driver.findElement(By.name("company_size")).sendKeys(CompanySize);
        driver.findElement(By.name("state")).sendKeys(StateRegion);
        driver.findElement(By.name("message")).sendKeys(AdditionalInformation);

        driver.findElement(By.className("actions")).click();

        Assert.assertTrue(driver.findElement(By.cssSelector(".submitted-message > p:nth-child(1)")).isDisplayed());
    }

    @DataProvider(name = "Authentication2")
    public static Object[][] credentials2() {
        return new Object[][]{
                {"test2", "test", "", "555-333-6666", "Test", "55", "CA", "This is a test. Please ignore"}};
    }

    @Test(dataProvider = "Authentication2")
    public void testTax2(String Username, String JobTitle, String EMail, String Phone, String Company, String CompanySize, String StateRegion, String AdditionalInformation) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[name='firstname']"))).sendKeys(Username);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[name='jobtitle']"))).sendKeys(JobTitle);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[name='email']"))).sendKeys(EMail);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[name='phone']"))).sendKeys(Phone);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[name='company']"))).sendKeys(Company);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[name='company_size']"))).sendKeys(CompanySize);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[name='state']"))).sendKeys(StateRegion);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[name='message']"))).sendKeys(AdditionalInformation);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[class='actions'"))).click();

        Assert.assertTrue(driver.findElement(By.cssSelector(".hs-main-font-element")).isDisplayed());
    }

    @DataProvider(name = "Authentication3")
    public static Object[][] credentials3() {
        return new Object[][]{
                {"test3", "test", "test@test.com ", "", "Test", "55", "CA", "This is a test. Please ignore"}};
    }

    @Test(dataProvider = "Authentication3")
    public void testTax3(String Username, String JobTitle, String EMail, String Phone, String Company, String CompanySize, String StateRegion, String AdditionalInformation) {
        driver.findElement(By.cssSelector("[name='firstname']")).sendKeys(Username);
        driver.findElement(By.cssSelector("[name='jobtitle']")).sendKeys(JobTitle);
        driver.findElement(By.cssSelector("[name='email']")).sendKeys(EMail);
        driver.findElement(By.cssSelector("[name='phone'")).sendKeys(Phone);
        driver.findElement(By.cssSelector("[name='company']")).sendKeys(Company);
        driver.findElement(By.cssSelector("[name='company_size']")).sendKeys(CompanySize);
        driver.findElement(By.cssSelector("[name='state']")).sendKeys(StateRegion);
        driver.findElement(By.cssSelector("[name='message']")).sendKeys(AdditionalInformation);

        driver.findElement(By.cssSelector("[class='actions'")).click();

        Assert.assertTrue(driver.findElement(By.cssSelector(".submitted-message > p:nth-child(1)")).isDisplayed());
    }

    @DataProvider(name = "Authentication4")
    public static Object[][] credentials4() {
        return new Object[][]{{"test4", "test", "", "", "Test", "55", "CA", "This is a test. Please ignore"}};
    }

    @Test(dataProvider = "Authentication4")
    public void testTax4(String Username, String JobTitle, String EMail, String Phone, String Company, String CompanySize, String StateRegion, String AdditionalInformation) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[name='firstname']"))).sendKeys(Username);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[name='jobtitle']"))).sendKeys(JobTitle);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[name='email']"))).sendKeys(EMail);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[name='phone']"))).sendKeys(Phone);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[name='company']"))).sendKeys(Company);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[name='company_size']"))).sendKeys(CompanySize);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[name='state']"))).sendKeys(StateRegion);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[name='message']"))).sendKeys(AdditionalInformation);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("actions"))).click();

        Assert.assertTrue(driver.findElement(By.cssSelector("[class='hs-main-font-element']")).isDisplayed());
    }
}
